import { Body, Controller, Post, UseGuards, UsePipes } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { JwtGuard } from '../auth/jwtGuard';
import { CurrentUser } from '../auth/current-user.decorator';
import { TokenPayload } from '../auth/jwt.strategy';
import { z } from 'zod';
import { Validate } from '../pipes/zod-validation-pipe';
import CreateQuestionUseCase from '../usecases/create-question.usecase';
import QuestionRepository from '../../infrastrucutre/repository/question.repository';

const createQuestionBodySchema = z.object({
  title: z.string(),
  content: z.string(),
});

type CreateQuestionBody = z.infer<typeof createQuestionBodySchema>;

@Controller('/questions/create')
@UseGuards(JwtGuard)
export default class CreateQuestionController {
  constructor(private readonly createQuestion: CreateQuestionUseCase) {}

  @Post()
  // @UsePipes(new Validate(createQuestionBodySchema))
  async handle(
    @Body() body: CreateQuestionBody,
    @CurrentUser() user: TokenPayload,
  ) {
    const input = {
      title: body.title,
      content: body.content,
      authorId: user.sub,
    };
    return this.createQuestion.execute(input);
  }
}
