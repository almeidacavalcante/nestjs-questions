import { Body, Controller, HttpCode, Post, UsePipes } from '@nestjs/common';
import { PrismaService } from '../../infrastrucutre/prisma/prisma.service';
import AccountRepository from '../../infrastrucutre/repository/account.repository';
import CreateAccountUseCase, {
  Input,
} from '../usecases/create-account.usecase';
import { z, ZodObject } from 'zod';
import { Validate } from '../pipes/zod-validation-pipe';

const createAccountSchema = z.object({
  name: z.string(),
  email: z.string().email(),
  password: z.string().min(6),
});

type CreateAccountSchema = z.infer<typeof createAccountSchema>;

@Controller('/accounts')
export default class CreateAccountController {
  constructor(private createAccount: CreateAccountUseCase) {}

  @Post()
  @HttpCode(201)
  @UsePipes(new Validate(createAccountSchema))
  async handle(@Body() body: Input) {
    const { name, email, password } = body;
    await this.createAccount.execute(name, email, password);
  }
}
