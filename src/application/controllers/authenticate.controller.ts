import { Body, Controller, HttpCode, Post, UsePipes } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import AuthenticateUseCase from '../usecases/authenticate.usecase';
import { Validate } from '../pipes/zod-validation-pipe';
import { z } from 'zod';

const authenticateBodySchema = z.object({
  email: z.string().email(),
  password: z.string().min(8),
});

type AuthenticateBodySchema = z.infer<typeof authenticateBodySchema>;

@Controller('/sessions')
export default class AuthenticateController {
  constructor(private readonly authenticateUseCase: AuthenticateUseCase) {}

  @Post()
  @HttpCode(200)
  @UsePipes(new Validate(authenticateBodySchema))
  async handle(@Body() body: any) {
    const input = {
      email: body.email,
      password: body.password,
    };
    return this.authenticateUseCase.execute(input);
  }
}
