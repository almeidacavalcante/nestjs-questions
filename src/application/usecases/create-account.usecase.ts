import Account from '../../domain/entities/account.entiity';
import Password from '../../domain/entities/password';
import Email from '../../domain/entities/email';
import Name from '../../domain/entities/name';
import AccountRepository from '../../infrastrucutre/repository/account.repository';
import { Injectable } from '@nestjs/common';
import ApplicationException from '../exceptions/application-exception';

@Injectable()
export default class CreateAccountUseCase {
  constructor(private readonly accountRepository: AccountRepository) {}

  async execute(
    name: string,
    email: string,
    password: string,
  ): Promise<Account> {
    const existingAccount = await this.accountRepository.findByEmail(email);
    if (existingAccount) {
      throw new ApplicationException('Account already exists');
    }

    const account = Account.create(
      new Name(name),
      new Email(email),
      new Password(password),
    );
    console.log(account);
    await this.accountRepository.save(account);
    return account;
  }
}

export type Input = {
  name: string;
  email: string;
  password: string;
};
