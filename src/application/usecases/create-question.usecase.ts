import AccountRepository from '../../infrastrucutre/repository/account.repository';
import { Injectable } from '@nestjs/common';
import Question from '../../domain/entities/question.entity';
import QuestionRepository from '../../infrastrucutre/repository/question.repository';

@Injectable()
export default class CreateQuestionUseCase {
  constructor(private readonly repository: QuestionRepository) {}

  async execute(input: Input): Promise<Question> {
    const question = Question.create(
      input.title,
      input.content,
      input.authorId,
    );

    await this.repository.save(question);

    return question;
  }
}

export type Input = {
  title: string;
  content: string;
  authorId: string;
};
