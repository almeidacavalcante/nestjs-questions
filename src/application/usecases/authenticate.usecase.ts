import { Injectable, UnauthorizedException } from '@nestjs/common';
import AccountRepository from '../../infrastrucutre/repository/account.repository';
import Account from '../../domain/entities/account.entiity';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export default class AuthenticateUseCase {
  constructor(
    private readonly accountRepository: AccountRepository,
    private readonly jwtService: JwtService,
  ) {}

  async execute(input: Input) {
    const existingAccount = await this.accountRepository.findUniqueByEmail(
      input.email,
    );

    if (!existingAccount) {
      throw new UnauthorizedException('Invalid account credentials');
    }

    const isPasswordValid = await existingAccount.password.isValidPassword(
      input.password,
    );

    if (!isPasswordValid) {
      throw new UnauthorizedException('Invalid account credentials');
    }

    const accessToken = this.jwtService.sign({ sub: existingAccount.id });
    return { access_token: accessToken };
  }
}

export type Input = {
  email: string;
  password: string;
};
