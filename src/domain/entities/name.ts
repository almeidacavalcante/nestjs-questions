export default class Name {
  constructor(readonly value: string) {
    if (!this.validate(value)) {
      throw new Error(`Invalid name: ${value}`);
    }
  }

  private validate(name: string): boolean {
    const re = /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/;
    return re.test(name);
  }
}
