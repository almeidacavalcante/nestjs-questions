export default class Email {
  constructor(readonly value: string) {
    if (!this.validate(value)) {
      throw new Error(`Invalid email: ${value}`);
    }
  }

  private validate(email: string): boolean {
    const re = /\S+@\S+\.\S+/;
    return re.test(email);
  }
}
