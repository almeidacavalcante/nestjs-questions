import Slug from './slug';

export default class Question {
  private constructor(
    readonly _title: string,
    readonly _content: string,
    readonly _authorId: string,
    readonly _slug: Slug,
    readonly _id?: string,
    readonly _createdAt?: Date,
    readonly _updatedAt?: Date,
  ) {}

  static create(title: string, content: string, authorId: string): Question {
    const slug = new Slug(title);
    return new Question(title, content, authorId, slug);
  }

  static restore(
    title: string,
    content: string,
    id: string,
    slug: Slug,
    authorId: string,
    createdAt: Date,
    updatedAt: Date,
  ): Question {
    return new Question(
      title,
      content,
      authorId,
      slug,
      id,
      createdAt,
      updatedAt,
    );
  }

  get title(): string {
    return this._title;
  }

  get content(): string {
    return this._content;
  }

  get slug(): string {
    return this._slug.value;
  }

  get id(): string | undefined {
    return this._id;
  }

  get createdAt(): Date | undefined {
    return this._createdAt;
  }

  get updatedAt(): Date | undefined {
    return this._updatedAt;
  }

  get authorId(): string {
    return this._authorId;
  }
}
