import DomainException from '../exceptions/domain-exception';
import { compare, hash } from 'bcryptjs';

export default class Password {
  protected _value: string;
  constructor(value: string) {
    if (!this.validate(value)) {
      throw new DomainException(`Invalid password: ${value}`);
    }

    this._value = value;
  }

  private validate(password: string): boolean {
    const re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/;
    return re.test(password);
  }

  public get value(): Promise<string> {
    return hash(this._value, 8);
  }

  public async isValidPassword(password: string): Promise<boolean> {
    return compare(password, this._value);
  }
}
