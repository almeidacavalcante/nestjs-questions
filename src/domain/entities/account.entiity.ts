import Email from './email';
import Name from './name';
import Password from './password';

export default class Account {
  private constructor(
    readonly _name: Name,
    readonly _email: Email,
    readonly _password: Password,
    readonly _id?: string,
    readonly _createdAt?: Date,
    readonly _updatedAt?: Date,
  ) {}

  static create(name: Name, email: Email, password: Password): Account {
    return new Account(name, email, password);
  }

  static restore(
    name: Name,
    email: Email,
    password: Password,
    id: string,
    createdAt: Date,
    updatedAt: Date,
  ): Account {
    return new Account(name, email, password, id, createdAt, updatedAt);
  }

  get name(): string {
    return this._name.value;
  }

  get email(): string {
    return this._email.value;
  }

  get password(): Password {
    return this._password;
  }

  get id(): string | undefined {
    return this._id;
  }

  get createdAt(): Date | undefined {
    return this._createdAt;
  }

  get updatedAt(): Date | undefined {
    return this._updatedAt;
  }
}
