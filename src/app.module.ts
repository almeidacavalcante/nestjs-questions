import { Module } from '@nestjs/common';

import { PrismaService } from './infrastrucutre/prisma/prisma.service';
import CreateAccountController from './application/controllers/create-account.controller';
import AccountRepository from './infrastrucutre/repository/account.repository';
import CreateAccountUseCase from './application/usecases/create-account.usecase';
import { ConfigModule } from '@nestjs/config';
import { envSchema } from './env';
import AuthenticateController from './application/controllers/authenticate.controller';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { AuthModule } from './application/auth/auth.module';
import AuthenticateUseCase from './application/usecases/authenticate.usecase';
import { JwtStrategy } from './application/auth/jwt.strategy';
import CreateQuestionController from './application/controllers/create-question.controller';
import CreateQuestionUseCase from './application/usecases/create-question.usecase';
import QuestionRepository from './infrastrucutre/repository/question.repository';

@Module({
  imports: [
    ConfigModule.forRoot({
      validate: (env) => envSchema.parse(env),
      isGlobal: true,
    }),
    AuthModule,
  ],
  controllers: [
    CreateAccountController,
    AuthenticateController,
    CreateQuestionController,
  ],
  providers: [
    JwtStrategy,
    PrismaService,
    AccountRepository,
    QuestionRepository,
    CreateAccountUseCase,
    AuthenticateUseCase,
    CreateQuestionUseCase,
  ],
})
export class AppModule {}
