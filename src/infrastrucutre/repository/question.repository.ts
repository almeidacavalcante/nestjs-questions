import { PrismaService } from '../prisma/prisma.service';
import { Injectable } from '@nestjs/common';
import Question from '../../domain/entities/question.entity';

@Injectable()
export default class QuestionRepository {
  constructor(readonly prisma: PrismaService) {}

  async save(question: Question) {
    await this.prisma.question.create({
      data: {
        title: question.title,
        content: question.content,
        slug: question.slug,
        authorId: question.authorId,
      },
    });
  }

  async deleteBySlug(slug: string) {
    await this.prisma.question.delete({
      where: {
        slug,
      },
    });
  }
}
