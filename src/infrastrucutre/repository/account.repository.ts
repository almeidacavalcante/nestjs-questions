import { PrismaService } from '../prisma/prisma.service';
import Account from '../../domain/entities/account.entiity';
import Name from '../../domain/entities/name';
import Email from '../../domain/entities/email';
import Password from '../../domain/entities/password';
import { Injectable } from '@nestjs/common';

@Injectable()
export default class AccountRepository {
  constructor(readonly prisma: PrismaService) {}

  async save(account: Account) {
    await this.prisma.user.create({
      data: {
        name: account.name,
        email: account.email,
        password: await account.password.value,
      },
    });
  }

  async findByEmail(email: string): Promise<Account | null> {
    const user = await this.prisma.user.findUnique({
      where: {
        email,
      },
    });

    if (!user) {
      return null;
    }

    return Account.create(
      new Name(user.name),
      new Email(user.email),
      new Password(user.password),
    );
  }

  async findUniqueByEmail(email: string) {
    const user = await this.prisma.user.findUnique({
      where: {
        email,
      },
    });

    if (!user) {
      return null;
    }

    return Account.restore(
      new Name(user.name),
      new Email(user.email),
      new Password(user.password),
      user.id,
      new Date(user.createdAt),
      new Date(user.updatedAt),
    );
  }

  async deleteByEmail(email: string) {
    await this.prisma.user.delete({
      where: {
        email,
      },
    });
  }
}
