import { Test } from '@nestjs/testing';
import { AppModule } from '../../src/app.module';
import { INestApplication } from '@nestjs/common';
import { PrismaService } from '../../src/infrastrucutre/prisma/prisma.service';
import AccountRepository from '../../src/infrastrucutre/repository/account.repository';
import { createTestAccount } from './create-account.controller.spec';
import * as request from 'supertest';

describe('Authenticate controller test', () => {
  let app: INestApplication;
  let prismaService = new PrismaService();
  let repository: AccountRepository = new AccountRepository(prismaService);

  async function removeAccount(email: string) {
    await repository.deleteByEmail(email);
    let account = await repository.findByEmail(email);
    expect(account).toBeFalsy();
  }

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  it('should authenticate an account', async () => {
    const { email, response } = await createTestAccount(app);

    let account = await repository.findByEmail(email);
    expect(response.status).toBe(201);

    const response2 = await request(app.getHttpServer())
      .post('/sessions')
      .send({
        email,
        password: 'PBC6fjiz',
      });

    expect(response2.status).toBe(200);
    expect(response2.body).toHaveProperty('access_token');
  });

  it('should return 401 for wrong-password', async () => {
    const { email, response } = await createTestAccount(app);

    let account = await repository.findByEmail(email);
    expect(response.status).toBe(201);

    const response2 = await request(app.getHttpServer())
      .post('/sessions')
      .send({
        email,
        password: 'wrong-password',
      });

    expect(response2.status).toBe(401);
  });

  it('should return 401 for wrong email', async () => {
    const email = 'wrong.email@email.com.br';
    const response2 = await request(app.getHttpServer())
      .post('/sessions')
      .send({
        email,
        password: 'PBC6fjiz',
      });

    expect(response2.status).toBe(401);
  });
});
