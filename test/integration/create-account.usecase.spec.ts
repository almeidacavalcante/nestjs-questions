import CreateAccountUseCase from '../../src/application/usecases/create-account.usecase';
import AccountRepository from '../../src/infrastrucutre/repository/account.repository';
import { PrismaService } from '../../src/infrastrucutre/prisma/prisma.service';
import { randomEmail } from '../test-utils';

describe('Create Account UseCase', () => {
  it('should create an account successfully', async () => {
    const ormService = new PrismaService();
    const repository = new AccountRepository(ormService);
    const createAccountUseCase = new CreateAccountUseCase(repository);

    const name = 'John Doe';
    const email = randomEmail();
    const password = 'PBC6fjiz';
    const account = await createAccountUseCase.execute(name, email, password);
    repository.save(account);
    expect(account).toBeTruthy();

    const retrievedAccount = await repository.findByEmail(email);
    expect(retrievedAccount).toBeTruthy();
    expect(retrievedAccount?.name).toBe(name);
    expect(retrievedAccount?.email).toBe(email);
  });
});
