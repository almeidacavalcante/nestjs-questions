import * as request from 'supertest';
import { INestApplication } from '@nestjs/common';
import { AppModule } from '../../src/app.module';
import { Test } from '@nestjs/testing';
import { randomEmail } from '../test-utils';
import AccountRepository from '../../src/infrastrucutre/repository/account.repository';
import { PrismaService } from '../../src/infrastrucutre/prisma/prisma.service';

let createTestAccount: (app: INestApplication<any>) => Promise<any>;
createTestAccount = async (app: INestApplication<any>) => {
  const email = randomEmail();
  const [response] = await Promise.all([
    request(app.getHttpServer()).post('/accounts').send({
      name: 'John Doe',
      email,
      password: 'PBC6fjiz',
    }),
  ]);
  return { email, response };
};
export { createTestAccount };

describe('Create account controller test', () => {
  let app: INestApplication;
  let prismaService = new PrismaService();
  let repository: AccountRepository = new AccountRepository(prismaService);
  async function removeAccount(email: string) {
    await repository.deleteByEmail(email);
    let account = await repository.findByEmail(email);
    expect(account).toBeFalsy();
  }

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  it('should create an account', async () => {
    const { email, response } = await createTestAccount(app);

    let account = await repository.findByEmail(email);
    expect(response.status).toBe(201);
    expect(account).toBeTruthy();
    await removeAccount(email);
  });

  it('should not create an account because it already exists (email)', async () => {
    const email = randomEmail();
    await request(app.getHttpServer()).post('/accounts').send({
      name: 'John Doe',
      email,
      password: 'PBC6fjiz',
    });

    const response = await request(app.getHttpServer()).post('/accounts').send({
      name: 'John Doe',
      email,
      password: 'PBC6fjiz',
    });

    expect(response.status).toBe(400);
    expect(response.body).toEqual({
      error: 'Bad Request',
      message: 'Account already exists',
      statusCode: 400,
    });

    await removeAccount(email);
  });

  it('should not create account with invalid password', async () => {
    const [response] = await Promise.all([
      request(app.getHttpServer()).post('/accounts').send({
        name: 'John Doe',
        email: randomEmail(),
        password: 'aaaaaa',
      }),
    ]);

    expect(response.status).toBe(400);
    expect(response.body).toEqual({
      error: 'Bad Request',
      message: 'Invalid password: aaaaaa',
      statusCode: 400,
    });
  });
});
