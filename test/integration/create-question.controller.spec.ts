import { INestApplication } from '@nestjs/common';
import { PrismaService } from '../../src/infrastrucutre/prisma/prisma.service';
import AccountRepository from '../../src/infrastrucutre/repository/account.repository';
import { Test } from '@nestjs/testing';
import { AppModule } from '../../src/app.module';
import * as request from 'supertest';
import { randomEmail } from '../test-utils';
import { createTestAccount } from './create-account.controller.spec';

describe('Create Question Controller Test', () => {
  let app: INestApplication;
  let prismaService = new PrismaService();
  let repository: AccountRepository = new AccountRepository(prismaService);

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  it('should create a question', async () => {
    const { email, response } = await createTestAccount(app);

    let account = await repository.findByEmail(email);
    expect(response.status).toBe(201);

    const response2 = await request(app.getHttpServer())
      .post('/sessions')
      .send({
        email,
        password: 'PBC6fjiz',
      });

    expect(response2.body).toHaveProperty('access_token');
    const token = response2.body.access_token;
    const [response3] = await Promise.all([
      request(app.getHttpServer())
        .post('/questions/create')
        .set('Authorization', `Bearer ${token}`)
        .send({
          title: 'How to create a question?',
          content: 'I am having trouble creating a question. Please help!',
        }),
    ]);
    console.log(response2.body);
    expect(response3.status).toBe(201);
  });

  it('should not allow unauthorized connections', async () => {
    const [response] = await Promise.all([
      request(app.getHttpServer()).post('/questions/create').send(),
    ]);
    expect(response.status).toBe(401);
  });
});
